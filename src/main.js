// src/main.js

const express = require("express");
const makeStoppable = require("stoppable");
const http = require("http");
const path = require("path");
const jwtSecretKey = 'your-secret-key';

const UsersService = require('./services/usersService');
const TasksService = require('./services/tasksService');
const AuthService = require('./services/authService');

const usersService = new UsersService();
const tasksService = new TasksService();
const authService = new AuthService(jwtSecretKey);

const authRoutes = require('./routes/auth');
const doneRoutes = require('./routes/done');
const tasksRoutes = require('./routes/tasks');  // Bu satırı ekleyin

const app = express();
const server = makeStoppable(http.createServer(app));
const port = 3000;

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'assets')));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use((req, res, next) => {
  if (req.path === '/api/auth/register' || req.path === '/api/auth/login') {
    next();
  } else {
    const token = req.headers.authorization ? req.headers.authorization.split(' ')[1] : null;
    if (!token) {
      return res.status(401).json({ message: 'Unauthorized. Token not provided.' });
    }

    try {
      const decodedToken = authService.verifyToken(token);
      req.user = decodedToken;
      next();
    } catch (error) {
      return res.status(401).json({ message: 'Unauthorized. Invalid token.' });
    }
  }
});

app.use((req, res, next) => {
  if (req.user && req.user.email) {
    res.locals.user = usersService.findUserByEmail(req.user.email);
    if (res.locals.user) {
      res.locals.userTasks = tasksService.getAllTasks(req.user.email);
    }
  }
  next();
});

app.use('/api/auth', authRoutes);
app.use('/done', doneRoutes);
app.use('/api/tasks', tasksRoutes); // Bu satırı ekleyin

server.listen(port, () => {
  console.log(`Express server is listening on http://localhost:${port}`);
});

module.exports = () => {
  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    });
  };

  return new Promise((resolve) => {
    resolve(stopServer);
  });
};
