// src/routes/auth.js

const express = require('express');
const router = express.Router();

const UsersService = require('../services/usersService');
const AuthService = require('../services/authService');

const jwtSecretKey = 'your-secret-key';
const usersService = new UsersService();
const authService = new AuthService(jwtSecretKey);

// Mevcut register endpoint
router.post('/register', async (req, res) => {
  const { email, password, firstName, lastName } = req.body;

  try {
    const user = await usersService.registerUser(email, password, firstName, lastName);
    res.status(200).json({ message: 'A user was successfully registered.'});
  } catch (error) {
    res.status(400).json({ message: 'Registration failed.', error: error.message });
  }
});

// Yeni login endpoint
router.post('/login', async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await usersService.findUserByEmail(email);
    if (!user) {
      return res.status(400).send('User with provided credentials does not exist.');
    }

    const passwordMatch = await authService.comparePasswords(password, user.password);
    if (!passwordMatch) {
      return res.status(400).send('User with provided credentials does not exist.');
    }

    const token = authService.generateToken({ email: user.email });
    res.status(200).json({ message: 'A user was successfully logged in. Returns a JWT token and a user object without a password.', 
    
      user: {
        email: user.email,
      },
      token,
    });
  } catch (error) {
    res.status(500).send('Something went wrong on the server.');
  }
});

module.exports = router;
