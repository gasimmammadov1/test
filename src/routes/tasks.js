const express = require('express');
const router = express.Router();

const TasksService = require('../services/tasksService');
const AuthService = require('../services/authService');

const jwtSecretKey = 'your-secret-key';
const tasksService = new TasksService();
const authService = new AuthService(jwtSecretKey);

// Auth middleware to verify token
const authMiddleware = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decoded = authService.verifyToken(token);
    req.user = decoded;
    next();
  } catch (error) {
    res.status(401).send('Authorization validation failed.');
  }
};

router.use(authMiddleware);

// Create a new task
router.post('/', async (req, res) => {  // <-- Yolu burada değiştirdik.
  try {
    const { title, description } = req.body;
    const task = await tasksService.addTask(req.user.email, title, description); // Email kullanıcıdan alınır
    res.status(201).json({ message: 'A TODO task was successfully created for a user.' });
  } catch (error) {
    res.status(500).send('Internal Server Error.');
  }
});

// Add other tasks related endpoints...

module.exports = router;
