// src/services/userServices.js

const bcrypt = require('bcrypt');

class UsersService {
    constructor() {
        // Geçici olarak kullanıcıları bellekte saklayalım.
        this.users = [];
    }

    async registerUser(email, password, firstName, lastName) {
        // Kullanıcının daha önce kayıtlı olup olmadığını kontrol edelim.
        const existingUser = this.users.find(user => user.email === email);
        if (existingUser) {
            throw new Error('Email already registered.');
        }

        // Check if the password length is at least 6 characters
        if (password.length < 6) {
            throw new Error('Email field contains an invalid email, or the length of a password is less than 6 characters. Also, when any of the fields are missing.');
        }

        // Hash the password before saving it
        const hashedPassword = await this.hashPassword(password);

        const newUser = {
            email,
            password: hashedPassword,
            firstName,
            lastName,
        };

        // Yeni kullanıcıyı oluşturalım ve listeye ekleyelim.
        this.users.push(newUser);

        return newUser;
    }

    async findUserByEmail(email) {
        // Kullanıcıyı email adresine göre bulalım.
        return this.users.find(user => user.email === email);
    }

    async hashPassword(password) {
        return bcrypt.hash(password, 10);
    }

}

module.exports = UsersService;
