// src/services/authServices.js

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');


class AuthService {
  constructor(secretKey) {
    this.secretKey = secretKey;
  }

  generateToken(payload) {
    return jwt.sign(payload, this.secretKey);
  }

  verifyToken(token) {
    return jwt.verify(token, this.secretKey);
  }

  async hashPassword(password) {
    return bcrypt.hash(password, 10);
  }

  async comparePasswords(password, hashedPassword) {
    return bcrypt.compare(password, hashedPassword);
  }
}

module.exports = AuthService;
