// src/services/tasksServices.js

class TasksService {
  constructor() {
    this.tasks = [];
  }

  async addTask(owner, title, description) {
    const existingTask = this.tasks.find(task => task.owner === owner && task.title === title);
    if (existingTask) {
      throw new Error('Task with the same title already exists.');
    }

    const newTask = {
      owner,
      title,
      description,
      done: false,
    };
    this.tasks.push(newTask);

    return newTask;
  }

  async getAllTasks(owner) {
    return this.tasks.filter(task => task.owner === owner);
  }

  async getCompletedTasks(owner) {
    return this.tasks.filter(task => task.owner === owner && task.done);
  }

  async markTaskAsDone(owner, title) {
    const task = this.tasks.find(task => task.owner === owner && task.title === title);
    if (!task) {
      throw new Error('Task not found.');
    }

    task.done = true;
    return task;
  }

  async updateTask(owner, title, newTitle, description) {
    const task = this.tasks.find(task => task.owner === owner && task.title === title);
    if (!task) {
      throw new Error('Task not found.');
    }

    // Check if a task with the new title already exists for the same user
    const existingTask = this.tasks.find(task => task.owner === owner && task.title === newTitle);
    if (existingTask && existingTask.title !== title) {
      throw new Error('Task with the new title already exists.');
    }

    task.title = newTitle;
    task.description = description;
    return task;
  }

  async deleteTask(owner, title) {
    const index = this.tasks.findIndex(task => task.owner === owner && task.title === title);
    if (index === -1) {
      throw new Error('Task not found.');
    }

    this.tasks.splice(index, 1);
  }
}

module.exports = TasksService;
